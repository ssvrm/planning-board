var express = require('express');
var app = express();

// Load Express Configuration
require('./expressConfig')(app, express);

// Root route
app.get('/', function(req, res) {
  res.sendFile('index.html', {root: app.settings.views});
});

// Root route
app.get('/api/:name', function(req, res) {
  res.sendFile(req.params.name + '.json', {root: app.settings.api});
});

// Load routes
module.exports = app;