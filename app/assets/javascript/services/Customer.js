angular.module('SSVRP_PlanBoard').factory('Customer', function(appConfig,$resource){
  return $resource(appConfig.apiURL + '/customers', {}, {});
});
