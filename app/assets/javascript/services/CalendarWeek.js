angular.module('SSVRP_PlanBoard').factory('CalendarWeek', function(appConfig,$resource){
  return $resource(appConfig.apiURL + '/calendarweeks', {}, {});
});
