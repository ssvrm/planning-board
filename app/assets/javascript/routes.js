angular.module('SSVRP_PlanBoard').config(function($routeProvider){
  $routeProvider
    .when('/', {
      redirectTo: '/board'
    })

    .when('/board', {
      templateUrl: "assets/templates/index.html",
      controller: "PlanBoardIndexController"
    })


});